package mis.pruebas.servicios.impl;

import mis.pruebas.modelos.Cliente;
import mis.pruebas.modelos.Cuenta;
import mis.pruebas.servicios.ObjetoNoEncontrado;
import mis.pruebas.servicios.ServicioCliente;
import mis.pruebas.servicios.ServicioCuenta;
import mis.pruebas.servicios.repositorios.RepositorioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    @Autowired
    RepositorioCuenta repositorioCuenta;

    @Autowired
    ServicioCliente servicioCliente;

    @Override
    public List<Cuenta> obtenerCuentas() {
        return this.repositorioCuenta.findAll();
    }

    @Override
    public void insertarCuentaNueva(Cuenta cuenta) {
       this.repositorioCuenta.insert(cuenta);
    }

    @Override
    public Cuenta obtenerCuenta(String numero) {
        final Optional<Cuenta> cuentaQuizas = this.repositorioCuenta.findById(numero);
        if(!cuentaQuizas.isPresent())
            throw new ObjetoNoEncontrado("La cuenta con el numero: "+numero+ " no existe");
        return  cuentaQuizas.get();
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {
        if(!this.repositorioCuenta.existsById(cuenta.numero))
            throw new ObjetoNoEncontrado("La cuenta con el numero: "+cuenta.numero+ " no existe");
        this.repositorioCuenta.save(cuenta);
    }

    @Override
    public void emparcharCuenta(Cuenta parche) {
        final Cuenta existente = obtenerCuenta(parche.numero);
        if(!this.repositorioCuenta.existsById(parche.numero))
            throw new ObjetoNoEncontrado("La cuenta con el numero: "+parche.numero+ " no existe");

        if(parche.moneda != null)
            existente.moneda = parche.moneda;

        if(parche.estado != null)
            existente.estado = parche.estado;

        if (parche.saldo != null)
            existente.saldo = parche.saldo;

        if (parche.oficina != null)
            existente.oficina = parche.oficina;

        if (parche.tipo != null)
            existente.tipo = parche.tipo;

        this.repositorioCuenta.save(existente);
    }

    @Override
    public void borrarCuenta(String numero) {
        if(!this.repositorioCuenta.existsById(numero))
            throw new ObjetoNoEncontrado("La cuenta con el numero: "+numero+ " no existe");
        this.repositorioCuenta.deleteById(numero);
    }
    @Override
    public Cuenta obtenerCuentaCliente(String documento, String numeroCuenta){

        if(!this.repositorioCuenta.existsById(numeroCuenta))
            throw new ObjetoNoEncontrado("No existe la cuenta con numero: "+ numeroCuenta);
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);

        if(!cliente.codigosCuentas.contains(numeroCuenta))
            throw new ObjetoNoEncontrado("La cuenta no esta asociada al cliente");
        final var cuenta= this.repositorioCuenta.findById(numeroCuenta);

        if (!cuenta.isPresent())
            throw new ObjetoNoEncontrado("La cuenta con numero: "+numeroCuenta+ " no existe");

        return cuenta.get();
    }

    @Override
    public void eliminarCuentaCliente(String documento, String numeroCuenta){
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
        final Optional<Cuenta> cuentaQuizas = this.repositorioCuenta.findById(numeroCuenta);
        if (!cuentaQuizas.isPresent())
            throw new ObjetoNoEncontrado("La cuenta con numero: "+ numeroCuenta + " no existe");

        if(!cliente.codigosCuentas.contains(numeroCuenta))
            throw new ObjetoNoEncontrado("La cuenta no esta asociada al cliente");

        final var cuenta= cuentaQuizas.get();
        cuenta.estado = "INACTIVA";
        cliente.codigosCuentas.remove(numeroCuenta);
        this.servicioCliente.guardarCliente(cliente);
        guardarCuenta(cuenta);

    }

    @Override
    public void reemplazarCuentaCliente(String documento, Cuenta cuenta) {
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
        final Optional<Cuenta> cuentaQuizas = this.repositorioCuenta.findById(cuenta.numero);
        if (!cuentaQuizas.isPresent())
            throw new ObjetoNoEncontrado("La cuenta con numero: "+cuenta.numero+ " no existe");
        if (!cliente.codigosCuentas.contains(cuenta.numero))
            throw new ObjetoNoEncontrado("La cuenta no esta asociada al cliente");
        guardarCuenta(cuenta);
    }

    @Override
    public void emparcharCuentaCliente(String documento, Cuenta cuenta) {
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
        final Optional<Cuenta> cuentaQuizas = this.repositorioCuenta.findById(cuenta.numero);
        if (!cuentaQuizas.isPresent())
            throw new ObjetoNoEncontrado("La cuenta con numero: "+cuenta.numero+ " no existe");
        if (!cliente.codigosCuentas.contains(cuenta.numero))
            throw new ObjetoNoEncontrado("La cuenta no esta asociada al cliente");
        emparcharCuenta(cuenta);
    }

}
