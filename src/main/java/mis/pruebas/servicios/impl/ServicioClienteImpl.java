package mis.pruebas.servicios.impl;

import mis.pruebas.modelos.Cliente;
import mis.pruebas.modelos.Cuenta;
import mis.pruebas.servicios.ObjetoNoEncontrado;
import mis.pruebas.servicios.ServicioCliente;
import mis.pruebas.servicios.repositorios.RepositorioCliente;
import mis.pruebas.servicios.repositorios.RepositorioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    @Autowired
    RepositorioCliente repositorioCliente;

    @Autowired
    RepositorioCuenta repositorioCuenta;

    @Override
    public Page<Cliente> obtenerClientes(int pagina, int cantidad) {
        return this.repositorioCliente.findAll(PageRequest.of(pagina, cantidad));    }

    @Override
    public List<Cliente> obtenerClientesTotales() {
        return this.repositorioCliente.findAll();
    }


    @Override
    public void insertarClienteNuevo(Cliente cliente) {
        this.repositorioCliente.insert(cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        final  Optional<Cliente> clienteQuizas = this.repositorioCliente.findById(documento);
        if(!clienteQuizas.isPresent())
            throw new ObjetoNoEncontrado("No existe el cliente con el documento: " +documento );
        return  clienteQuizas.get();
    }

    @Override
    public void guardarCliente(Cliente cliente) {
        if(!this.repositorioCliente.existsById(cliente.documento))
            throw new ObjetoNoEncontrado("No existe el cliente con el documento: "+ cliente.documento);
        this.repositorioCliente.save(cliente);
    }

    @Override
    public void emparcharCliente(Cliente parche) {
        final Cliente existente = obtenerCliente(parche.documento);

        if(!this.repositorioCliente.existsById(parche.documento))
            throw new ObjetoNoEncontrado("No existe el cliente con el documento: "+ parche.documento);
        if(parche.edad != null)
            existente.edad = parche.edad;

        if(parche.nombre != null)
            existente.nombre = parche.nombre;

        if(parche.correo != null)
            existente.correo = parche.correo;

        if(parche.direccion != null)
            existente.direccion = parche.direccion;

        if(parche.telefono != null)
            existente.telefono = parche.telefono;

        if(parche.fechaNacimiento != null)
            existente.fechaNacimiento = parche.fechaNacimiento;


        this.repositorioCliente.save(existente);
    }

    @Override
    public void borrarCliente(String documento) {
        if (!this.repositorioCliente.existsById(documento))
            throw new ObjetoNoEncontrado("No se puede eliminar el cliente con documento: "+ documento+ " ya que no existe");
        this.repositorioCliente.deleteById(documento);
    }

    @Override
    public void agregarCuentaCliente(String documento, String numeroCuenta) {
         final Cliente cliente = obtenerCliente(documento);
         if(!this.repositorioCuenta.existsById(numeroCuenta))
             throw new ObjetoNoEncontrado("No existe la cuenta numero: "+ numeroCuenta);
         cliente.codigosCuentas.add(numeroCuenta);

         if(!this.repositorioCliente.existsById(documento))
            throw new ObjetoNoEncontrado("No existe cliente con el documento: "+ documento);

         this.repositorioCliente.save(cliente);
    }

    @Override
    public List<String> obtenerCuentasCliente(String documento) {
        final Cliente cliente = obtenerCliente(documento);
        return  cliente.codigosCuentas;
    }

}
