package mis.pruebas.servicios;

import mis.pruebas.modelos.Cliente;
import mis.pruebas.modelos.Cuenta;
import mis.pruebas.modelos.Cuenta;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ServicioCuenta {

    public List<Cuenta> obtenerCuentas();

    //CREAR
    public void insertarCuentaNueva(Cuenta cuenta);

    //READ
    public Cuenta obtenerCuenta(String numero);

    //UPDATE
    public void guardarCuenta(Cuenta cuenta);
    public void emparcharCuenta(Cuenta parche);

    //DELETE
    public  void borrarCuenta(String numero);

    public Cuenta obtenerCuentaCliente(String documento, String numeroCuenta);

    public void eliminarCuentaCliente(String documento, String numeroCuenta);

    public void reemplazarCuentaCliente(String documento, Cuenta cuenta);

    public void emparcharCuentaCliente(String documento, Cuenta cuenta);



}
