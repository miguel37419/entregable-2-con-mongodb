package mis.pruebas.servicios.repositorios;

import mis.pruebas.modelos.Cliente;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioCliente extends MongoRepository<Cliente,String> {
}
