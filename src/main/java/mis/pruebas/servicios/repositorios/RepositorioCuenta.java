package mis.pruebas.servicios.repositorios;

import mis.pruebas.modelos.Cuenta;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioCuenta extends MongoRepository<Cuenta,String> {
}
